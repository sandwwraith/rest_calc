package com.sandwwraith.calc.client;

import com.sandwwraith.calc.service.CalcResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface to create GET request to the REST calculator
 * from it via Retrofit framework.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/
public interface CalcRequest {

    /**
     * Method to create a request to REST calculator.
     * <p>
     * Use Retrofit with JSON deserializer to parse the answer to instance of {@link CalcResult}
     *
     * @param expression Expression to evaluate.
     * @return Instance of {@link Call} which can be used to start a network request.
     */
    @GET("calc")
    Call<CalcResult> getResult(@Query("expr") String expression);
}

package com.sandwwraith.calc.client;

import com.sandwwraith.calc.service.CalcResult;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Main client application class for interacting with REST calculator.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 */
public class App {
    /**
     * Base URI of the service.
     */
    public static final String BASE_URI = com.sandwwraith.calc.service.Main.BASE_URI; //"http://localhost:8080/rest-calc/";

    /**
     * Creates an instance of {@link CalcRequest}, which allows
     * you to make requests to the REST service
     * and deserialize JSON answer.
     *
     * @return Created instance.
     */
    public static CalcRequest createRequest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URI)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        return retrofit.create(CalcRequest.class);
    }

    /**
     * Entry point for console client.
     * Client reads user input line-by-line, each line is sent to the server
     * as an arithmetic expression to evaluate. After each line, result of evaluation is printed.
     *
     * @param args No command-line args supported; this argument is ignored.
     * @throws IOException If an I/O error occurred.
     */
    public static void main(String[] args) throws IOException {
        CalcRequest service = createRequest();

        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter(Pattern.compile("\r\n?|\n"));
        while (scanner.hasNext()) {
            String s = scanner.next();
            CalcResult result = service.getResult(s).execute().body();
            if (result.hasNoErrors()) {
                System.out.println(result.getResult());
            } else {
                System.out.println("ERRORS: " + result.getErrors().toString());
            }
        }
    }
}

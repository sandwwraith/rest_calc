package com.sandwwraith.calc.client;

import com.sandwwraith.calc.service.CalcResult;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class AppTest {
    private static CalcRequest request;
    private static HttpServer server;

    /**
     * Starting the server and creating request
     */
    @BeforeClass
    public static void setUp() {
        request = App.createRequest();
        server = com.sandwwraith.calc.service.Main.startServer();
    }

    @AfterClass
    public static void tearDown() {
        server.shutdownNow();
    }

    /**
     * Test normal request
     */
    @Test
    public void test2Plus2() throws IOException {
        CalcResult result = request.getResult("2+2").execute().body();
        assertEquals(4, result.getResult());
    }

    /**
     * Test invalid expression
     */
    @Test
    public void testInvalid() throws IOException {
        CalcResult result = request.getResult("2+2*(2").execute().body();
        assertFalse(result.hasNoErrors());
    }
}

grammar Expression;

expression: prior1 EOF;

prior1 : prior2 #PriorityTwo
			| prior1 '+' prior2 #Add
			| prior1 '-' prior2 #Subtract
			;

prior2 : term #Single
		| prior2 '*' term #Multiply
		| prior2 '/' term #Divide
		;

term: NUMBER #Number
	| '-'term #Negation
	| '('prior1')' #Parenthesis
	;

NUMBER : DIGIT +;
DIGIT : ('0'..'9');
WS : [ \t\r]+ -> skip ;
package com.sandwwraith.calc.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.List;

/**
 * Represent result of evaluating arithmetic expression.
 * Serializable/Deserializable to JSON with <tt>Jackson</tt>.
 * <p>
 * Always contains boolean field <code>noErrors</code>.
 * If this field is false, contains integer field <code>result</code>
 * with result of evaluating.
 * If <code>noErrors</code> is true, contains <code>errors</code> -
 * a list of strings with errors description.
 * <p>
 * A proper way to work with this class: first check {@link #hasNoErrors()}
 * and then, based on the result, call {@link #getResult()} or {@link #getErrors()}.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE)
public class CalcResult {
    private boolean noErrors;
    private Integer result;
    private List<String> errors;

    /**
     * Default constructor is necessary for deserialization from JSON.
     * Does nothing.
     */
    private CalcResult() {
    }

    /**
     * Creates the successful result
     *
     * @param result Result of evaluating
     */
    CalcResult(Integer result) {
        this.noErrors = true;
        this.result = result;
    }

    /**
     * Creates the failed result
     *
     * @param errors Messages of errors occurred during the evaluation
     */
    CalcResult(List<String> errors) {
        this.noErrors = false;
        this.errors = errors;
    }

    /**
     * Creates the failed result.
     *
     * @param error Message about single error.
     */
    CalcResult(String error) {
        this(Collections.singletonList(error));
    }

    /**
     * Checks whether this result is successful.
     *
     * @return True if this object contains result, false if it contains errors.
     */
    public boolean hasNoErrors() {
        return noErrors;
    }

    /**
     * Gets the result from this object.
     *
     * @return Result of evaluating some expression.
     * @throws IllegalStateException If this object contains errors instead of result.
     */
    public int getResult() throws IllegalStateException {
        if (result == null) {
            throw new IllegalStateException("Can't get result: there are errors");
        }
        return result;
    }

    /**
     * Gets errors occurred during evaluation of expression.
     *
     * @return List of error messages.
     * @throws IllegalStateException If this object contains no errors and represents valid result.
     */
    public List<String> getErrors() throws IllegalStateException {
        if (errors == null) {
            throw new IllegalStateException("Result finished without errors");
        }
        return errors;
    }
}

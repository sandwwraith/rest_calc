package com.sandwwraith.calc.service;

import com.sandwwraith.calc.service.expression.Calculator;
import com.sandwwraith.calc.service.expression.CalculatorException;
import org.glassfish.jersey.server.ManagedAsync;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

/**
 * JAX-RS resource for evaluating arithmetic expressions.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 */
@Path("calc")
public class CalcResource {

    static final String NO_EXPR_MSG = "No expression passed";

    /**
     * Method handles <tt>HTTP GET</tt> requests.
     * Parses and calculates given expression asynchronously.
     * Returns {@link CalcResult} object in JSON format.
     *
     * @param expression A string with expression to evaluate.
     *                   Expression format described in JavaDoc for {@link Calculator}.
     * @param response   Provides support for async request.
     */
    @GET
    @ManagedAsync
    @Produces(MediaType.APPLICATION_JSON)
    public void evaluate(@QueryParam("expr") String expression, @Suspended final AsyncResponse response) {
        CalcResult result;
        if (expression == null || expression.isEmpty()) {
            result = new CalcResult(NO_EXPR_MSG);
        } else {
            try {
                result = new CalcResult(Calculator.evaluate(expression));
            } catch (CalculatorException e) {
                result = new CalcResult(e.getErrors());
            } catch (Exception e) {
                result = new CalcResult(e.getMessage());
            }
        }
        response.resume(result);
    }
}

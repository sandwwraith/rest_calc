package com.sandwwraith.calc.service.expression;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.misc.Nullable;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.function.BinaryOperator;

/**
 * Provides static method for evaluating arithmetic expressions.
 * <p>
 * Expression may contain:
 * <ul>
 * <li>spaces and tabs (ignored)</li>
 * <li>simple arithmetic operators as + , - , * , / </li>
 * <li>parenthesis and unary '-'</li>
 * </ul>
 * Only integer arithmetic supported.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/
public class Calculator {

    /**
     * Parses and calculates given expression.
     *
     * @param expression Arithmetic expression, in format described in {@link Calculator}.
     * @return Result of expression.
     * @throws CalculatorException If parsing of expression unsuccessful,
     *                             or if integer overflow happened in process of calculating.
     */
    public static Integer evaluate(String expression) throws CalculatorException {
        ErrorCollector errors = new ErrorCollector();

        //lexer creation
        ExpressionLexer lexer = new ExpressionLexer(
                new ANTLRInputStream(
                        expression));
        lexer.removeErrorListeners();
        lexer.addErrorListener(errors);

        //parser creation
        ExpressionParser parser = new ExpressionParser(
                new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(errors);
        parser.setErrorHandler(new BailErrorStrategy());

        //walking parse tree
        ExprWalker extractor = new ExprWalker();
        try {
            ParseTreeWalker.DEFAULT.walk(extractor, parser.expression());
        } catch (ParseCancellationException | NumberFormatException | ArithmeticException error) {
            errors.addError(error.getMessage());
        }
        if (errors.hasErrors()) {
            throw new CalculatorException(errors.getErrors());
        }
        return extractor.getResult();
    }

    /**
     * Error listener for {@link ExpressionLexer} and {@link ExpressionParser}.
     * Collects error messages to list.
     */
    private static class ErrorCollector extends BaseErrorListener {
        private final List<String> errors = new ArrayList<>();

        /**
         * <p>
         * Adds a {@code msg} to list, with a note about {@code charPositionInLine}
         * </p>
         * {@inheritDoc}
         */
        @Override
        public void syntaxError(@NotNull Recognizer<?, ?> recognizer, @Nullable Object offendingSymbol, int line, int charPositionInLine, @NotNull String msg, @Nullable RecognitionException e) {
            errors.add(String.format("%s at position %d", msg, charPositionInLine));
        }

        boolean hasErrors() {
            return errors.size() != 0;
        }

        List<String> getErrors() {
            return Collections.unmodifiableList(errors);
        }

        void addError(String errorMsg) {
            errors.add(errorMsg);
        }
    }

    /**
     * Listener for walking parse tree produced by {@link ExpressionParser}.
     * Default walker traverses the tree with DFS, so this listener computes
     * parsed expression on the stack.
     */
    private static class ExprWalker extends ExpressionBaseListener {
        private final Stack<Integer> stack = new Stack<>();

        /**
         * Pops the top two elements of the stack, performs operation
         * on them and pushes the result back.
         * Top of the stack will be second operand,
         * and element below the top will be first operand.
         *
         * @param op Operation to perform.
         */
        private void perform(BinaryOperator<Integer> op) {
            Integer b = stack.pop();
            Integer a = stack.pop();
            stack.push(op.apply(a, b));
        }

        @Override
        public void exitDivide(@NotNull ExpressionParser.DivideContext ctx) {
            perform((a, b) -> {
                if (a == Integer.MIN_VALUE && b == -1) {
                    throw new ArithmeticException("integer overflow");
                }
                return a / b;
            });
        }

        @Override
        public void exitMultiply(@NotNull ExpressionParser.MultiplyContext ctx) {
            perform(Math::multiplyExact);
        }

        @Override
        public void exitSubtract(@NotNull ExpressionParser.SubtractContext ctx) {
            perform(Math::subtractExact);
        }

        @Override
        public void exitAdd(@NotNull ExpressionParser.AddContext ctx) {
            perform(Math::addExact);
        }

        @Override
        public void exitNegation(@NotNull ExpressionParser.NegationContext ctx) {
            stack.push(-stack.pop());
        }

        @Override
        public void exitNumber(@NotNull ExpressionParser.NumberContext ctx) {
            stack.push(Integer.parseInt(ctx.getText()));
        }

        /**
         * Returns the result of computing expression, which must be on the top of stack.
         *
         * @return the result
         */
        public Integer getResult() {
            return stack.pop();
        }
    }
}

package com.sandwwraith.calc.service.expression;

import java.util.Collections;
import java.util.List;

/**
 * Occurs in process of lexing or parsing expression,
 * or if arithmetic error happened during calculating expression.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/
public class CalculatorException extends Exception {
    private final List<String> errors;

    /**
     * Creates exception with single error message.
     *
     * @param error Detailed error message.
     */
    public CalculatorException(String error) {
        this(Collections.singletonList(error));
    }

    /**
     * Creates exception with list of error messages.
     *
     * @param errors Errors happened in process of evaluating some expression.
     */
    public CalculatorException(List<String> errors) {
        super(errors.toString());
        this.errors = errors;
    }

    /**
     * Gets all errors caused this exception.
     *
     * @return List of error messages.
     */
    public List<String> getErrors() {
        return errors;
    }
}

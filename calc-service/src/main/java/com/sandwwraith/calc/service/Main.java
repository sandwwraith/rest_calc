package com.sandwwraith.calc.service;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

/**
 * Main class for server application
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 */
public class Main {
    /**
     * Base URI the Grizzly HTTP server will listen on
     */
    public static final String BASE_URI = "http://localhost:8080/rest-calc/";

    /**
     * Starts Grizzly HTTP server with JAX-RS resources provided
     * in {@link com.sandwwraith.calc.service} package
     *
     * @return Started grizzly HTTP server.
     */
    public static HttpServer startServer() {

        final ResourceConfig rc = new ResourceConfig()
                .packages("com.sandwwraith.calc.service")
                .register(JacksonFeature.class); //Jackson support

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Entry point for application.
     * Starts the server and awaits the <code>ENTER</code> hit to shutdown it.
     *
     * @param args No command-line args supported; this argument is ignored.
     * @throws IOException If an I/O error occurs in {@link System#in}.
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format(
                "REST calculator service started at %scalc%sHit enter to stop it..."
                , BASE_URI, System.lineSeparator()));
        //noinspection ResultOfMethodCallIgnored
        System.in.read();
        server.shutdownNow();
    }
}


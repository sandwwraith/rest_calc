package com.sandwwraith.calc.service.expression;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for expression evaluating.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/
public class CalculatorTest {

    private int count(String expr) throws CalculatorException {
        return Calculator.evaluate(expr);
    }

    @Test
    public void count1() throws Exception {
        assertEquals(8, count("2+2+2+2"));
    }

    @Test
    public void count2() throws Exception {
        assertEquals(5, count("2+(-2+2)+1+-2+4"));
    }

    @Test
    public void count3() throws Exception {
        assertEquals(9, count("5+7-(2+2)+1"));
    }

    @Test
    public void count4() throws Exception {
        assertEquals(6, count("2+2*2"));
    }

    @Test
    public void count5() throws Exception {
        assertEquals(2138, count("22+48*15/(25--4+-27)*(2+2*2)-44"));
    }

    @Test(expected = CalculatorException.class)
    public void overflow1() throws Exception {
        count(String.format("%d+%d", Integer.MAX_VALUE, 1));
    }

    @Test(expected = CalculatorException.class)
    public void overflow2() throws Exception {
        count(String.format("%d*%d", Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2));
    }

    @Test(expected = CalculatorException.class)
    public void parserFail() throws Exception {
        count("2+2(*");
    }

    @Test(expected = CalculatorException.class)
    public void lexerFail() throws Exception {
        count("2+2*a2f");
    }

}
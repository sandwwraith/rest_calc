package com.sandwwraith.calc.service;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class CalcResourceTest {

    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        target = c.target(Main.BASE_URI);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        server.shutdownNow();
    }

    /**
     * Test simple expression to check HTTP interaction
     */
    @Test
    public void test2Plus2() {
        CalcResult response = target.path("calc").queryParam("expr", "2+2").request().get(CalcResult.class);
        assertEquals(4, response.getResult());
    }

    /**
     * Test invalid expression
     */
    @Test
    public void testInvalidExpr() {
        CalcResult response = target.path("calc").queryParam("expr", "2+2*").request().get(CalcResult.class);
        assertFalse(response.hasNoErrors());
    }

    /**
     * Test behavior if no query param passed
     */
    @Test
    public void testNull() {
        CalcResult response = target.path("calc").request().get(CalcResult.class);
        assertFalse(response.hasNoErrors());
        assertEquals(CalcResource.NO_EXPR_MSG, response.getErrors().get(0));
    }
}
